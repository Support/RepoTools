# RepoTools

This repository contains four scripts useful for developing RISC OS with Git.
They require Bash and Python 3, which are both installed by default on most
versions of Linux and Mac OS. Windows users can obtain them here:

* http://git-scm.com/download/win - ensure "Git Bash Here" is ticked when you get
to the "Select Components" screen of the installer
* https://www.python.org/downloads - ensure you download the latest 3.x version
and select "Install for all users"

Clone this repository to your local machine. You must add its directory (and for
Windows users, the Python interpreter also) to your `PATH` so that the scripts
can be found.

For Linux:

```
git clone https://gitlab.riscosopen.org/Support/RepoTools.git ~/RepoTools
echo "PATH=\$PATH:\"$HOME/RepoTools\"" >> ~/.bashrc
```

For Windows, select "Git Bash Here" from any directory, then:

```
git clone https://gitlab.riscosopen.org/Support/RepoTools.git ~/RepoTools
echo "PATH=\$PATH:/c/Python27:\"$HOME/RepoTools\"" >> ~/.bash_profile
```

For Mac OS:

```
git clone https://gitlab.riscosopen.org/Support/RepoTools.git ~/RepoTools
echo "PATH=\$PATH:\"$HOME/RepoTools\"" >> ~/.profile
```

## srccommit

The srccommit script is a wrapper for `git commit` and it has a number of
advantages. It checks that you remembered to create a branch for your changes
(it is very important that you commit to a named branch, or your changes may be
overwritten and require advanced Git commands to retrieve). It creates or
updates the `VersionNum` (and optionally `VersionASM`) files in a format that
matches the rest of the RISC OS sources so that they contain today’s date and
an appropriate set of version numbers. Also, it provides a template for your
commit message to remind you how to write one so that it matches the style used
elsewhere in RISC OS.

For components where GitLab CI is enabled, a further benefit of using srccommit
(and of meaningful development branch names) will become apparent, since the
result will be that the branch name and revision number become embedded in the
version string of the binaries that are built whenever you push them to your
fork project.

## prodcommit

This script is used instead of `srccommit` when doing a commit that alters the
submodules in a Products super-project.

## BBCBasicToText.py

This Python script allows Git to display tokenised BBC BASIC files. There are
two steps to configuring it, firstly register it as a text converter:

    git config --global diff.basic.textconv BBCBasicToText.py

Secondly, you need to tell Git that files ending in `,ffb` are tokenised BASIC
files by adding the line `*,ffb diff=basic` to your global Git attributes file.
The location of this file is specified by the `core.attributesfile`
configuration option, which you can check with:

    git config --global core.attributesfile

Normally this will be unset, and the default location will be
`$XDG_CONFIG_HOME/git/attributes`. If `$XDG_CONFIG_HOME` is either not set or
empty, `$HOME/.config/git/attributes` is used instead.

For most people, `core.attributesfile` and `$XDG_CONFIG_HOME` will be unset, so
you will be able to add the attribute by using:

    echo "*,ffb diff=basic" >> $HOME/.config/git/attributes

## git-mtime.py

This Python program will recurse through a specified directory, or the current
directory, and set the modified time of each file (that is under git control)
to the time that file was last committed (modified) in git.

## diffrelease

This awk script is used to generate release notes. It is executed from within
a superproject, and given "before" and "after" tags for the superproject, it
outputs HTML (default) or plaintext information on stdout.
